using System;
using Xunit;

using CalculAiresConsole;

namespace CalculAiresTest
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            Assert.Equal(4, Program.CalculAireCarre(2));
            Assert.Equal(0, Program.CalculAireCarre(0));
            Assert.Equal(2116, Program.CalculAireCarre(46));
            Assert.Equal(0, Program.CalculAireCarre('a'));
            Assert.Equal(9, Program.CalculAireCarre(-3));
        }
    }
}
